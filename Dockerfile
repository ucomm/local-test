FROM node:8.10.0-alpine

WORKDIR /project

COPY ./.entrypoint/entrypoint.sh /entrypoint.sh

RUN apk update && apk add bash \
  && chmod +x /entrypoint.sh \
  && npm set progress=false \
  && npm install -g yarn gulp-cli

EXPOSE 3000 3001 35729

ENTRYPOINT [ "/entrypoint.sh" ]
