const gulp = require('gulp');
const browserSync = require('browser-sync');
const pluginOptions = {
  DEBUG: true,
  camelize: true,
  lazy: true
};
const plugins = require('gulp-load-plugins')(pluginOptions);

gulp.task('js', () => {
  return gulp.src('./src/js/**/*.js')
    .pipe(gulp.dest('build', {
      overwrite: true
    }))
    .pipe(browserSync.stream())
});

gulp.task('sass', () => {
  return gulp.src('./src/sass/**/*.scss')
    .pipe(plugins.sass())
    .pipe(gulp.dest('build', {
      overwrite: true
    }))
    .pipe(browserSync.stream())
});

gulp.task('watch', () => {
  console.log('watching');
  const files = [
    './src/sass/**/*.scss',
    './src/js/**/*.js'
  ];
  browserSync.init(files, {
    proxy: "web:80",
    notify: true,
    open: false
  })
  gulp.watch(['./src/sass/**/*.scss'], ['sass']);
  gulp.watch(['./src/js/**/*.js'], ['js']);

});

gulp.task('default', ['sass', 'js'], () => {
  console.log('default task ran');
});